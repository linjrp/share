FROM node:18.17.0-alpine3.18 AS build-env
WORKDIR /usr/src/app/
COPY src/ ./
RUN npm install

FROM gcr.io/distroless/nodejs18-debian11:nonroot
HEALTHCHECK NONE
USER 65532
WORKDIR /usr/src/app/
COPY --from=build-env /usr/src/app/ ./
EXPOSE 8080
CMD ["ratings.js", "8080"]
